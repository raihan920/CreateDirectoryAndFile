package com.example.raihan.createdirectoryandfile;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String myCustomFolder = Environment.getExternalStoragePublicDirectory("My folder").getAbsolutePath();
        final String fileName = "my_data.txt";
        final File directory = new File(myCustomFolder);
        final File file = new File(myCustomFolder, fileName);
        if(!directory.exists())
        {
            directory.mkdir();
        }
        if(!file.exists())
        {
            try {
                file.createNewFile();
            }
            catch (Exception ex)
            {
                Log.e(getPackageName(), "File create error "+ex.getMessage());
            }
        }

        /*-- Create and save file start --*/
        final EditText txtInput = (EditText) findViewById(R.id.editText1);
        Button myBtn1 = (Button) findViewById(R.id.button1);
        myBtn1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                try {
                    FileOutputStream stream = new FileOutputStream(file);
                    stream.write(txtInput.getText().toString().getBytes());
                    stream.close();
                    Toast.makeText(MainActivity.this, "Saved", Toast.LENGTH_SHORT).show();
                }
                catch (Exception ex)
                {
                    Log.e(getPackageName(), "write error" + ex.getMessage());
                }
            }
        });
        /*-- Create and save file end --*/

        /*-- Read Data From File Start --*/
        final EditText txtShow = (EditText) findViewById(R.id.editText2);
        Button myBtn2 = (Button) findViewById(R.id.button2);
        myBtn2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int length = (int) file.length();
                byte[] bytes = new byte[length];
                try {
                    FileInputStream in = new FileInputStream(file);
                    in.read(bytes);
                    in.close();
                }
                catch (Exception ex){
                    Log.e(getPackageName(), "read error "+ex.getMessage());
                }
                String contents = new String(bytes);
                txtShow.setText(contents);
            }
        });
        /*-- Read Data From File Ends --*/

        /* -- Permission starts here -- */
        if (Build.VERSION.SDK_INT >= 23)
        {
            if ((ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED))
            {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
            }
            else
            {
                //Toast.makeText(MainActivity.this, "Something went wrong !!", Toast.LENGTH_SHORT).show();
            }
        }
        /* -- Permission ends here -- */

        /* -- Loading Image Starts -- */
        Button showImgBtn = (Button) findViewById(R.id.button3);
        showImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    ImageView imgView = (ImageView) findViewById(R.id.imgView1);
                    Bitmap bitmap = BitmapFactory.decodeFile(Environment.getExternalStoragePublicDirectory
                            (Environment.DIRECTORY_PICTURES).getAbsolutePath()+"/IMG_1290.JPG"); // this is the picture directory in phone memory
                    imgView.setImageBitmap(bitmap);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        /* -- Loading Image Ends -- */
    }
}
